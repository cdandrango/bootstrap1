module.exports = function(grunt) {
 require('time-grunt')(grunt);
 require('jit-grunt')(grunt,{
   useminPrepare:'grunt-usemin'
 });
  grunt.initConfig({
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'css',
          src: ['*.scss'],
          dest:'css',
          ext: '.css'
        }]
      }

    },
    clean: {
      build: {
        src: ['/dist']
      }
    },
    copy: {
      html: {
        expand: true,
        cwd: '/',
        src: ['*.html'],
        dest: 'dist',
      },
    },
    watch: {
      files:['css/*.scss'],
      tasks:['css']
    },
    browserSync: {
      dev: {
          bsFiles: {
              src : [
                  'css/*.css',
                  '*.html'

              ]
          },
          options: {
              watchTask: true,
              server: './'
          }
        }
      },
      imagemin: {
        dynamic: {
          files: [{
              expand: true,
              cwd: 'src/',
              src: ['**/*.{png,jpg,gif}'],
              dest: 'dist/'
          }]
      }
      }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.registerTask('css',['sass']);
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('img:compress', ['imagemin']);

};    
